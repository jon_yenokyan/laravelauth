<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Image extends Model
{
    CONST PAGINATECOUNT = 12;

    protected $fillable = [
        'image',
        'uploader_id',
    ];

    public function uploader()
    {
        return $this->hasOne(User::class, 'id','uploader_id');
    }
}
