<?php

namespace App\Helper;

use App\User;
use Illuminate\Support\Facades\Cookie;
use Illuminate\Support\Facades\Session;

class AuthHelper
{
    private static $cookie = 'remember';
    private static $key = 'user_id';
    private static $minutes = 100000;

    public static function init()
    {
//        if (Session::has(self::$key)) {
//            $id = Session::get(self::$key);
//            $user = User::find($id);
//
//            self::login($user);
//        }

        $token = Cookie::get(self::$cookie);
        if (is_null($token)) {
            return;
        }

        $user = User::where('remember_token', $token)->first();
        Session::put(self::$key, $user->id);

        Cookie::queue(self::$cookie, $token, self::$minutes);
    }

    public static function login(User $user, $remember = false)
    {
        Session::put(self::$key, $user->id);
        if ($remember) {
            $token = str_random(64);
            $user->remember_token = $token;
            $user->save();

            Cookie::queue(self::$cookie, $token, self::$minutes);
        }
    }

    public static function logout()
    {
        $user = self::user();
        if (!is_null($user)) {
            $user->remember_token = null;
            $user->save();

            Session::forget(self::$key);
        }
    }

    public static function guest()
    {
        return !self::check();
    }

    public static function check()
    {
        return Session::has(self::$key);
    }

    public static function id()
    {
        return Session::get(self::$key);
    }

    public static function user()
    {
        return User::find(self::id());
    }
}