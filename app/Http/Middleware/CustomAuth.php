<?php

namespace App\Http\Middleware;

use App\Helper\AuthHelper;
use Closure;

class CustomAuth
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (AuthHelper::guest()) {
            return redirect()->route('login_view');
        }

        return $next($request);
    }
}
