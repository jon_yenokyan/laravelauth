<?php

namespace App\Http\Middleware;

use App\Helper\AuthHelper;
use Closure;

class CustomGuest
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (AuthHelper::check()) {
            return redirect('/');
        }

        return $next($request);
    }
}
