<?php

namespace App\Http\Middleware;

use App\Helper\AuthHelper;
use Closure;
use Illuminate\Session\Middleware\StartSession;

class InitAuthHelper extends StartSession
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        AuthHelper::init();
        return $next($request);
    }
}
