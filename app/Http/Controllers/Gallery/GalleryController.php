<?php

namespace App\Http\Controllers\Gallery;

use App\Helper\AuthHelper;
use App\Image;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\File;

class GalleryController extends Controller
{
    public function list()
    {
        $images = Image::orderBy('id','desc')->paginate(Image::PAGINATECOUNT);
        return view('gallery.list')
            ->with('images', $images);
    }

    public function delete($id)
    {
        
        $image = Image::find($id);
        if (is_null($image)|| $image->uploader->id != AuthHelper::id()) {
            return redirect()->route('gallery_list');
        }
        ////delete path
        $path = public_path('/uploads/' . $image->image);
        File::delete($path);
        $image->delete();

        return redirect()->route('gallery_list');
    }
}
