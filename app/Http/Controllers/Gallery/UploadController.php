<?php

namespace App\Http\Controllers\Gallery;

use App\Helper\AuthHelper;
use App\Image;
use App\User;
use App\Http\Controllers\Controller;
use Auth;
use Illuminate\Http\UploadedFile;
use Mail;
use Illuminate\Http\Request;

class UploadController extends Controller
{
    public function uploadView()
    {
        return view('gallery.upload');
    }

    public function uploadPost(Request$request)
    {
        $this->validate($request,[
            'images.*' => 'mimes:jpeg,png,jpg,gif,svg'
        ]);

        $images = $request->file('images');

        /** @var UploadedFile $image */
        foreach ($images as $image) {
            $photoName = uniqid() . '.' . $image->getClientOriginalExtension();
            $image->move(public_path('uploads'), $photoName);

            Image::create([
                'uploader_id' => AuthHelper::id(),
                'image' => $photoName,
            ]);
        }

        return redirect()->route('gallery_list');
    }
}
