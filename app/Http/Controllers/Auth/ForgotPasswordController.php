<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\User;
use DB;
use Mail;
use Illuminate\Http\Request;

class ForgotPasswordController extends Controller
{
    public function forgotView()
    {
        return view('auth.forgot');
    }

    public function forgotPost(Request $request)
    {
        $this->validate($request, [
            'email' => 'required|email'
        ]);

        $user = User::where('email', $request->email)->first();
        if (is_null($user)) {
            // incorrect email
            return redirect()->back()
                ->withInput($request->only('email'))
                ->withErrors([
                    'email' => 'Incorrect email',
                ]);
        }

        //make token
        $token = str_random(64);
        DB::table('password_resets')->insert([
            'email' => $request->get('email'),
            'token' => $token,
        ]);

        // send mail
        Mail::send('email.reset', ['user' => $user, 'token' => $token], function ($m) use ($user) {
            $m->from('no-reply@my-website.am', env('APP_NAME', 'Laravel'));
            $m->to($user->email, $user->name)
                ->subject('Reset Password');
        });

        return redirect('/');
    }
}
