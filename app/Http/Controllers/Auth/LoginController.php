<?php

namespace App\Http\Controllers\Auth;

use App\Helper\AuthHelper;
use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class LoginController extends Controller
{
    public function logout()
    {
        AuthHelper::logout();
        return redirect()->route('login_view');
    }

    public function loginView()
    {
        return view('auth.login');
    }

    public function loginPost(Request$request)
    {
        $user = User::where('email', $request->email)
            ->first();

        if (is_null($user)) {
            // incorrect email
            return redirect()->back()
                ->withInput($request->only('email', 'remember'))
                ->withErrors([
                    'email' => 'Incorrect email',
                ]);
        }

        if (!$user->verified) {
            // not verified
            return redirect()->back()
                ->withInput($request->only('email', 'remember'))
                ->withErrors([
                    'email' => 'Verify email',
                ]);
        }

        if (!Hash::check($request->password, $user->password)) {
            // incorrect password
            return redirect()->back()
                ->withInput($request->only('email', 'remember'))
                ->withErrors([
                    'password' => 'Incorrect password',
                ]);
        }

        // login
        AuthHelper::login($user, $request->has('remember'));
        return redirect('/');
    }
}
