<?php

namespace App\Http\Controllers\Auth;

use App\Helper\AuthHelper;
use App\Http\Controllers\Controller;
use App\User;
use DB;
use Illuminate\Http\Request;

class ResetPasswordController extends Controller
{
    public function resetView($token)
    {
        if (is_null($this->_dbRow($token)->first())) {
            return redirect('/');
        }

        return view('auth.reset')
            ->with('token', $token);
    }

    public function resetPost(Request $request)
    {
        $this->validate($request,[
            'password' => 'required|min:6|confirmed',
        ]);

        $reset = $this->_dbRow($request->token)->first();
        $user = User::where('email', $reset->email)->first();

        // update
        $user->password = bcrypt($request->password);
        $user->save();

        // delete token
        $this->_dbRow($request->token)->delete();

        // automatically login
        AuthHelper::login($user);
        return redirect('/');
    }

    private function _dbRow($token)
    {
        // get reset password token row
        return DB::table('password_resets')
            ->where('token', $token);
    }
}
