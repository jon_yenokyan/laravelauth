<?php

namespace App\Http\Controllers\Auth;

use App\Helper\AuthHelper;
use App\User;
use App\Http\Controllers\Controller;
use Mail;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class RegisterController extends Controller
{
    public function registerView()
    {
        return view('auth.register');
    }

    public function registerPost(Request $request)
    {
        $this->validate($request,[
            'name' => 'required|max:255',
            'email' => 'required|email|max:255|unique:users',
            'password' => 'required|min:6|confirmed',
        ]);

        $token = str_random(64);

        // register
        $user = User::create([
            'name' => $request->name,
            'email' => $request->email,
            'password' => bcrypt($request->password),
            'verify_token' => $token,
        ]);

        // send mail
        Mail::send('email.verify', ['user' => $user], function ($m) use ($user) {
            $m->from('no-reply@my-website.am', env('APP_NAME', 'Laravel'));
            $m->to($user->email, $user->name)
                ->subject('Verify Account');
        });

        return redirect()->route('login_view');
    }

    public function verify($token)
    {
        // get user by token
        $user = User::where('verify_token', $token)
            ->where('verified', false)
            ->first();

        if (is_null($user)) {
            // invalid token
            return redirect('/');
        }

        // update state
        $user->verified = true;
        $user->save();

        // automatically login
        AuthHelper::login($user);
        return redirect('/');
    }
}
