<?php

namespace App\Http\Controllers\Auth;


use App\Helper\AuthHelper;
use App\Http\Controllers\Controller;
use DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class ChangePasswordController extends Controller
{
    public function changeView()
    {
        return view('auth.change');
    }

    public function changePost(Request $request)
    {
        $this->validate($request,[
            'current' => 'required|min:6',
            'password' => 'required|min:6|confirmed',
        ]);

        $user = AuthHelper::user();
        if (Hash::check($request->current, $user->password)) {
            //update password
            $user->password = bcrypt($request->password);
            $user->save();

            return redirect('/');
        }

        // incorrect password
        return redirect()->back()
            ->withInput($request->only('current'))
            ->withErrors([
                'current' => 'Current password is incorrect',
            ]);
    }
}