<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect()->route('gallery_list');
});

Route::group(['namespace' => 'Auth'], function () {
    Route::get('/logout', 'LoginController@logout')->name('logout');

    Route::group(['middleware' => 'c_guest'], function () {
        Route::get('/login', 'LoginController@loginView')->name('login_view');
        Route::post('/login', 'LoginController@loginPost')->name('login');

        Route::get('/register', 'RegisterController@registerView')->name('register_view');
        Route::post('/register', 'RegisterController@registerPost')->name('register');
        Route::get('/verify/{token}', 'RegisterController@verify')->name('verify');

        Route::group(['prefix' => '/password'], function () {
            Route::get('/forgot', 'ForgotPasswordController@forgotView')->name('forgot_view');
            Route::post('/forgot', 'ForgotPasswordController@forgotPost')->name('forgot');

            Route::get('/reset/{token}', 'ResetPasswordController@resetView')->name('reset_view');
            Route::post('/reset', 'ResetPasswordController@resetPost')->name('reset');
        });
    });

    Route::group(['middleware' =>'c_auth'], function () {
        Route::get('/password-change', 'ChangePasswordController@changeView')->name('change_view');
        Route::post('/password-change', 'ChangePasswordController@changePost')->name('change');
    });
});

Route::group(['prefix' => '/gallery', 'namespace' => 'Gallery', 'middleware' => 'c_auth'], function () {
    Route::get('/upload', 'UploadController@uploadView')->name('upload_view');
    Route::post('/upload', 'UploadController@uploadPost')->name('gallery');

    Route::get('/list', 'GalleryController@list')->name('gallery_list');
    Route::get('/delete/{id}', 'GalleryController@delete')->name('delete_image');
});
