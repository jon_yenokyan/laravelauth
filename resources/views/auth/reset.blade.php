@extends('layouts.app')

@section('content')
    <div class="container">
        <div id="loginbox" style="margin-top:50px;" class="mainbox col-md-6 col-md-offset-3 col-sm-8 col-sm-offset-2">
            <div class="panel panel-info" >
                <div class="panel-heading">
                    <div class="panel-title">Reset</div>
                </div>

                <div style="padding-top:30px" class="panel-body" >

                    <div style="display:none" id="login-alert" class="alert alert-danger col-sm-12"></div>

                    <form id="loginform" class="form-horizontal" role="form" method="post" action="{{ route('reset') }}">
                        @csrf

                        <input type="hidden" name="token" value="{{ $token }}" />

                        <div style="margin-bottom: 25px" class="input-group">
                            <span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
                            <input id="login-username" type="password" class="form-control" name="password" placeholder="New Password">

                            @if ($errors->has('password'))
                                <strong class="errors">{{ $errors->first('password') }}</strong>
                            @endif
                        </div>

                        <div style="margin-bottom: 25px" class="input-group">
                            <span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
                            <input id="login-username" type="password" class="form-control" name="password_confirmation" placeholder="Reenter Password">
                        </div>


                        <div style="margin-top:10px" class="form-group">
                            <div class="col-sm-12 controls">
                                <button id="btn-login" class="btn btn-success">Reset </button>

                            </div>
                        </div>
                    </form>



                </div>
            </div>
        </div>
    </div>
@endsection