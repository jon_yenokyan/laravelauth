@extends('layouts.app')

@section('content')
<div class="container">
    <div id="loginbox" style="margin-top:50px;" class="mainbox col-md-6 col-md-offset-3 col-sm-8 col-sm-offset-2">
        <div class="panel panel-info" >
            <div class="panel-heading">
                <div class="panel-title">Sign Up</div>
            </div>

            <div style="padding-top:30px" class="panel-body" >

                <div style="display:none" id="login-alert" class="alert alert-danger col-sm-12"></div>

                <form class="" method="post" action="{{ route('register') }}">
                    @csrf

                    <div class="form-group">
                        <div class="row">
                            <div class="col-md-3">
                                <label for="name" class="control-label">Name</label>
                            </div>
                            <div class="col-md-9">
                                <input type="text" class="form-control" name="name" value="{{ old('name') }}" placeholder="Name">

                                @if ($errors->has('name'))
                                    <strong class="errors">{{ $errors->first('name') }}</strong>
                                @endif
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="row">
                            <div class="col-md-3">
                                <label for="email" class="control-label">Email</label>
                            </div>
                            <div class="col-md-9">
                                <input type="text" class="form-control" name="email" value="{{ old('email') }}" placeholder="Email Address">

                                @if ($errors->has('email'))
                                    <strong class="errors">{{ $errors->first('email') }}</strong>
                                @endif
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="row">
                            <div class="col-md-3">
                                <label for="password" class="control-label">Password</label>
                            </div>
                            <div class="col-md-9">
                                <input type="password" class="form-control" name="password" placeholder="Password">
                                @if ($errors->has('password'))
                                    <strong class="errors">{{ $errors->first('password') }}</strong>
                                @endif
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="row">
                            <div class="col-md-3">
                                <label for="password" class="control-label">Confirm</label>
                            </div>
                            <div class="col-md-9">
                                <input type="password" class="form-control" name="password_confirmation" placeholder="Confirm">
                                @if ($errors->has('password_confirmation'))
                                    <strong class="errors">{{ $errors->first('password_confirmation') }}</strong>
                                @endif
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="row">
                            <div class="col-md-offset-3 col-md-9">
                                <input type="submit" class="btn btn-info" value="Sign Up" />
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection