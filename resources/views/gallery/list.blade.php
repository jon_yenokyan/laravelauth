
@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h3>Gallery</h3>
                <a href="{{ route('upload_view') }}">Upload</a>
            </div>
        </div>

        <div class="row">
            @if(empty($images))
                <div class="col-md-12">
                    No Images To Show
                </div>
            @else
                <div class="col-md-12">
                    <div class="row">
                        @foreach($images as $image)
                            <div class="col-md-3">
                                <div class="image-item">
                                    <div class="image">
                                        <img src="{{ asset('/uploads/'. $image->image) }}" />
                                    </div>
                                    <div class="user">
                                        <span>{{ $image->uploader->name }}</span>
                                        @if ($image->uploader->id == \App\Helper\AuthHelper::id())
                                            <a style="float: right;" href="{{ route('delete_image', ['id' => $image->id]) }}">Delete</a>
                                        @endif
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    </div>
                    <div class="row">
                        {{ $images->links() }}
                    </div>
                </div>
            @endif
        </div>
    </div>
@endsection
