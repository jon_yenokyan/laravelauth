
@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="col-md-12">
            <form id="upload" method="post" action="{{ route('gallery') }}" enctype="multipart/form-data">
                @csrf

                <div class="form-group">
                    <div class="row">
                        <div class="col-md-6">
                            <span class="control-label">Browse</span>
                            <input type="file" name="images[]" multiple />
                            @foreach($errors->getBag('default')->all() as $error)
                                <strong class="errors"><span style="font-size: 18px ; color: black;">* </span> {{ $error }}</strong><br />
                            @endforeach
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="row">
                        <div class="col-md-6">
                            <input class="btn btn-default" type="submit"value="Submit" />
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
@endsection